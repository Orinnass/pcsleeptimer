﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCSleepTimer
{
    public partial class SleepTimerForm : Form
    {
        private uint timerValue = 0;

        public uint TimerValue
        {
            get { return timerValue; }
            private set
            {
                timerValue = value;
                var timerValueSpan = TimeSpan.FromSeconds(value);
                leftTimerValueLabel.Text = $"Осталось до сна: {timerValueSpan.Hours}:{timerValueSpan.Minutes}:{timerValueSpan.Seconds}";
            }
        }

        public SleepTimerForm()
        {
            InitializeComponent();
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            if ((TimerValue == 0) && (timerTextBox.Text == string.Empty))
            {
                MessageBox.Show("Значение для таймера не может быть пустым");
                return;
            }

            TimerValue = TimerValue != 0 ? TimerValue : Convert.ToUInt32(timerTextBox.Text);
            timer.Enabled = true;
            startButton.Enabled = false;
            pauseTimerButton.Enabled = true;
            offTimerButton.Enabled = true;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            TimerValue--;

            if (TimerValue == 0)
            {
                Application.SetSuspendState(PowerState.Suspend, false, true);
            }
        }

        private void OffTimerButton_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            startButton.Enabled = true;
            offTimerButton.Enabled = false;
            pauseTimerButton.Enabled = false;
            TimerValue = 0;
            leftTimerValueLabel.Text = "Таймер отключен";
        }

        private void PauseTimerButton_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            pauseTimerButton.Enabled = false;
            startButton.Enabled = true;
            var leftTimerValue = TimeSpan.FromSeconds(TimerValue);
            leftTimerValueLabel.Text = $"Таймер приостановлен: {leftTimerValue.Hours}:{leftTimerValue.Minutes}:{leftTimerValue.Seconds}";
        }
    }
}
