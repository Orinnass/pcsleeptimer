﻿namespace PCSleepTimer
{
    partial class SleepTimerForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startButton = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.timerTextBox = new System.Windows.Forms.TextBox();
            this.timerLabel = new System.Windows.Forms.Label();
            this.leftTimerValueLabel = new System.Windows.Forms.Label();
            this.pauseTimerButton = new System.Windows.Forms.Button();
            this.offTimerButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(175, 206);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Запустить";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // timerTextBox
            // 
            this.timerTextBox.Location = new System.Drawing.Point(166, 101);
            this.timerTextBox.Name = "timerTextBox";
            this.timerTextBox.Size = new System.Drawing.Size(100, 20);
            this.timerTextBox.TabIndex = 1;
            // 
            // timerLabel
            // 
            this.timerLabel.AutoSize = true;
            this.timerLabel.Location = new System.Drawing.Point(109, 85);
            this.timerLabel.Name = "timerLabel";
            this.timerLabel.Size = new System.Drawing.Size(241, 13);
            this.timerLabel.TabIndex = 2;
            this.timerLabel.Text = "Через сколько включить спящий режим (сек)";
            // 
            // leftTimerValueLabel
            // 
            this.leftTimerValueLabel.AutoSize = true;
            this.leftTimerValueLabel.Location = new System.Drawing.Point(12, 9);
            this.leftTimerValueLabel.Name = "leftTimerValueLabel";
            this.leftTimerValueLabel.Size = new System.Drawing.Size(97, 13);
            this.leftTimerValueLabel.TabIndex = 3;
            this.leftTimerValueLabel.Text = "Таймер отключен";
            // 
            // pauseTimerButton
            // 
            this.pauseTimerButton.Enabled = false;
            this.pauseTimerButton.Location = new System.Drawing.Point(256, 206);
            this.pauseTimerButton.Name = "pauseTimerButton";
            this.pauseTimerButton.Size = new System.Drawing.Size(75, 23);
            this.pauseTimerButton.TabIndex = 4;
            this.pauseTimerButton.Text = "Пауза";
            this.pauseTimerButton.UseVisualStyleBackColor = true;
            this.pauseTimerButton.Click += new System.EventHandler(this.PauseTimerButton_Click);
            // 
            // offTimerButton
            // 
            this.offTimerButton.Enabled = false;
            this.offTimerButton.Location = new System.Drawing.Point(94, 206);
            this.offTimerButton.Name = "offTimerButton";
            this.offTimerButton.Size = new System.Drawing.Size(75, 23);
            this.offTimerButton.TabIndex = 5;
            this.offTimerButton.Text = "Отключить";
            this.offTimerButton.UseVisualStyleBackColor = true;
            this.offTimerButton.Click += new System.EventHandler(this.OffTimerButton_Click);
            // 
            // SleepTimerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 241);
            this.Controls.Add(this.offTimerButton);
            this.Controls.Add(this.pauseTimerButton);
            this.Controls.Add(this.leftTimerValueLabel);
            this.Controls.Add(this.timerLabel);
            this.Controls.Add(this.timerTextBox);
            this.Controls.Add(this.startButton);
            this.Name = "SleepTimerForm";
            this.Text = "Sleep Timer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TextBox timerTextBox;
        private System.Windows.Forms.Label timerLabel;
        private System.Windows.Forms.Label leftTimerValueLabel;
        private System.Windows.Forms.Button pauseTimerButton;
        private System.Windows.Forms.Button offTimerButton;
    }
}

